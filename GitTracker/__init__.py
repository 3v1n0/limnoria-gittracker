# -*- Encoding: utf-8 -*-
###
# Copyright (c) 2005-2007 Dennis Kaarsemaker
# Copyright (c) 2008-2010 Terence Simpson
# Copyright (c) 2018, Marco Trevisan (Treviño)
# All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of version 2 of the GNU General Public License as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
###

"""
GitTracker: Provide informations from Git based trackers (Gitlab and GitHub)
"""

import sys
import supybot
from supybot import world

__version__ = "2.9.0"
__author__ = supybot.Author("Marco Trevisan (Treviño)", "Trevinho", "marco@ubuntu.com")
__contributors__ = {
    supybot.Author("Dennis Kaarsemaker","Seveas","dennis@kaarsemaker.net"): ['Original Author'],
    supybot.Author("Terence Simpson", "tsimpson", "tsimpson@ubuntu.com"): ['Previous Author']
}
__url__ = 'https://gitlab.gnome.org/3v1n0/limnoria-gittracker'

from . import config
from . import plugin
if sys.version_info >= (3, 4):
    from importlib import reload
else:
    from imp import reload
# In case we're being reloaded.
reload(config)
reload(plugin)
# Add more reloads here if you add third-party modules and want them to be
# reloaded when this plugin is reloaded.  Don't forget to import them as well!

if world.testing:
    from . import test

Class = plugin.Class
configure = config.configure


# vim:set shiftwidth=4 tabstop=4 expandtab textwidth=79:
