# -*- Encoding: utf-8 -*-
###
# Copyright (c) 2005-2007 Dennis Kaarsemaker
# Copyright (c) 2008-2011 Terence Simpson
# Copyright (c) 2018, Marco Trevisan (Treviño)
# All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of version 2 of the GNU General Public License as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
###

from supybot import conf, registry, ircutils
try:
    from supybot.i18n import PluginInternationalization
    _ = PluginInternationalization('GitTracker')
except:
    # Placeholder that allows to run the plugin on a bot
    # without the i18n module
    _ = lambda x: x


class GitTrackers(registry.SpaceSeparatedListOfStrings):
    List = ircutils.IrcSet

def configure(advanced):
    from supybot.questions import expect, anything, something, yn
    conf.registerPlugin('GitTracker', True)

    def anything(prompt, default=None):
        """Because supybot is pure fail"""
        from supybot.questions import expect
        return expect(prompt, [], default=default)

    GitTracker = conf.supybot.plugins.GitTracker

    def getRepeatdelay():
        output("How many seconds should the bot wait before repeating bug information?")
        repeatdelay = something("Enter a number greater or equal to 0", default=GitTracker.repeatdelay._default)

        try:
            repeatdelay = int(repeatdelay)
            if repeatdelay < 0:
                raise TypeError
        except TypeError:
            output("%r is an invalid value, it must be an integer greater or equal to 0" % repeatdelay)
            return getRepeatdelay()
        else:
            return repeatdelay

    output("Each of the next 3 questions can be set per-channel with the '@Config channel' command")
    if advanced:
        replyNoBugtracker = something("What should the bot reply with when a a user requests information from an unknown bug tracker?", default=GitTracker.replyNoBugtracker._default)
        # snarfTarget = something("What should be the default bug tracker used when one isn't specified?", default=GitTracker.snarfTarget._default)
        replyWhenNotFound = yn("Should the bot report when a bug is not found?", default=GitTracker.replyWhenNotFound._default)
        repeatdelay = getRepeatdelay()
    else:
        replyNoBugtracker = GitTracker.replyNoBugtracker._default
        # snarfTarget = GitTracker.snarfTarget._default
        replyWhenNotFound = GitTracker.replyWhenNotFound._default
        repeatdelay = GitTracker.repeatdelay._default

    showassignee = yn("Show the assignee of a bug in the reply?", default=GitTracker.showassignee._default)
    extended = yn("Show tracker-specific extended infomation?", default=GitTracker.extended._default)

    GitTracker.bugSnarfer.setValue(urlSnarfer)
    # GitTracker.bugSnarfer.setValue(bugSnarfer)
    # GitTracker.snarfTarget.setValue(snarfTarget)
    GitTracker.replyNoBugtracker.setValue(replyNoBugtracker)
    GitTracker.replyWhenNotFound.setValue(replyWhenNotFound)
    GitTracker.repeatdelay.setValue(repeatdelay)
    GitTracker.showassignee.setValue(showassignee)
    GitTracker.extended.setValue(extended)

GitTracker = conf.registerPlugin('GitTracker')

conf.registerChannelValue(GitTracker, 'urlSnarfer',
    registry.Boolean(True, """Determines whether the url snarfer will be
    enabled, such that any Git Tracker URLs seen in the channel
    will have their information reported into the channel."""))

# conf.registerChannelValue(GitTracker, 'bugSnarfer',
#     registry.Boolean(False, """Determines whether the bug snarfer will be
#     enabled, such that any bug reference seen in the channel
#     will have their information reported into the channel."""))

conf.registerChannelValue(GitTracker, 'replyNoBugtracker',
    registry.String('I don\'t have a bugtracker %s.', """Determines the phrase
    to use when notifying the user that there is no information about that
    bugtracker site."""))

conf.registerGlobalValue(GitTracker, 'gittrackers',
    GitTrackers([], """Determines what gittrackers will be added to the bot when it starts."""))

conf.registerGlobalValue(GitTracker, 'replyWhenNotFound',
    registry.Boolean(False, """Whether to send a message when a bug could not be
    found"""))

conf.registerChannelValue(GitTracker, 'repeatdelay',
    registry.Integer(60, """Number of seconds to wait between repeated bug calls"""))

conf.registerChannelValue(GitTracker, 'showassignee',
    registry.Boolean(False, """Whether to show the assignee in bug reports"""))

conf.registerChannelValue(GitTracker, 'extended',
    registry.Boolean(False, "Show optional extneded bug information, specific to trackers"))
