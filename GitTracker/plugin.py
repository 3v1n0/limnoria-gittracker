# -*- Encoding: utf-8 -*-
###
# Copyright (c) 2005-2007 Dennis Kaarsemaker
# Copyright (c) 2008-2010 Terence Simpson
# Copyright (c) 2018, Marco Trevisan (Treviño)
# All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of version 2 of the GNU General Public License as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
###

from supybot import utils, plugins, ircutils, callbacks, conf, registry, \
    schedule, log as supylog
from supybot.commands import *
try:
    from supybot.i18n import PluginInternationalization
    _ = PluginInternationalization('GitTracker')
except ImportError:
    # Placeholder that allows to run the plugin on a bot
    # without the i18n module
    _ = lambda x: x

import re, os, time

# All the words below will be censored when reporting bug information
bad_words = set(['fuck', 'fuk', 'fucking', 'fuking', 'fukin', 'fuckin',
                 'fucked', 'fuked', 'fucker', 'shit', 'cunt', 'bastard',
                 'nazi', 'nigger', 'nigga', 'cock', 'bitches', 'bitch'])

def makeClean(s):
    words = s.split()
    for word in words:
        if word.lower() in bad_words:
            words[words.index(word)] = "<censored>"
    return " ".join(words)

def registerGitTracker(name, url='', description='', trackertype='', apikey=''):
    gittrackers = conf.supybot.plugins.GitTracker.gittrackers
    gittrackers().add(name)
    group       = conf.registerGroup(gittrackers, name)
    URL         = conf.registerGlobalValue(group, 'url', registry.String(url, ''))
    DESC        = conf.registerGlobalValue(group, 'description', registry.String(description, ''))
    TRACKERTYPE = conf.registerGlobalValue(group, 'trackertype', registry.String(trackertype, ''))
    API_KEY     = conf.registerGlobalValue(group, 'apikey', registry.String(apikey, ''))
    if url:
        URL.setValue(url)
    if description:
        DESC.setValue(description)
    if not apikey:
        apikey = gittrackers.get(name).apikey()
    if apikey:
        API_KEY.setValue(apikey)
    elif trackertype == 'gitlab':
        raise GitTrackerError("Impossible to use a gitlab tracker without API key: %s" % url)
    if trackertype:
        if trackertype.lower() in list(defined_gittrackers.keys()):
            TRACKERTYPE.setValue(trackertype.lower())
        else:
            raise GitTrackerError("Unknown trackertype: %s" % trackertype)

def tryRegisterGitTracker(name, url='', description='', trackertype='', apikey=''):
    try:
        registerGitTracker(name, url, description, trackertype, apikey)
    except GitTrackerError as e:
        supylog.error("Impossible to register tracker {}: {}".format(name, e))

class GitTrackerError(Exception):
    """A gittracker error"""
    pass

class BugNotFoundError(Exception):
    """Pity, bug isn't there"""
    pass

class GitTracker(callbacks.PluginRegexp):
    """Provide informations from Git based trackers (Gitlab and GitHub)"""
    threaded = True
    callBefore = ['URL']
    regexps = ['urlSnarfer', 'bugSnarfer']

    def __init__(self, irc):
        callbacks.PluginRegexp.__init__(self, irc)
        self.db = ircutils.IrcDict()
        for name in self.registryValue('gittrackers'):
            registerGitTracker(name)
            group = self.trackerConfigGroup(name)
            if group.trackertype() in list(defined_gittrackers):
                tracker = defined_gittrackers[group.trackertype()]
                self.db[name] = tracker(name, group.url(), group.description())
            else:
                self.log.warning("GitTracker: Unknown trackertype: %s (%s)" % (group.trackertype(), name))
        self.shorthand = utils.abbrev(list(self.db.keys()))
        self.shown = {}

    def trackerConfigGroup(self, name):
        return self.registryValue('gittrackers.%s' % name.replace('.','\\.'), value=False)

    def die(self): #TODO: Remove me
        pass

    def is_ok(self, channel, tracker, bug):
        '''Flood/repeat protection'''
        now = time.time()
        for k in list(self.shown):
            if self.shown[k] < now - self.registryValue('repeatdelay', channel):
                self.shown.pop(k)
        if (channel, tracker, bug) not in self.shown:
            self.shown[(channel, tracker, bug)] = now
            return True
        return False

    def add(self, irc, msg, args, name, trackertype, url, apikey, description):
        """<name> <type> <url> <api key> [<description>]

        Add a bugtracker <url> to the list of defined gittrackers. <type> is the
        type of the tracker (currently only github and gitlab are known).
        <name> is the name that will be used to reference the tracker in all
        commands.
        Unambiguous abbreviations of <name> will be accepted also.
        <description> is the common name for the tracker and will be listed with
        the tracker query; if not given, it defaults to <name>.
        <api key> is the tracker API key for the tracker, currently needed for
        any gitlab instance.
        """
        name = name.lower()
        if not description:
            description = name
        if url[-1] == '/':
            url = url[:-1]
        trackertype = trackertype.lower()
        if trackertype in defined_gittrackers:
            tracker = defined_gittrackers[trackertype]
            self.db[name] = tracker(name, url, description)
        else:
            irc.error("GitTracker of type '%s' are not understood" % trackertype)
            return
        registerGitTracker(name, url, description, trackertype, apikey)
        self.shorthand = utils.abbrev(list(self.db.keys()))
        irc.replySuccess()
    add = wrap(add, [('checkCapability', 'admin'), 'something', 'something', 'url', additional('text')])

    def remove(self, irc, msg, args, name):
        """<abbreviation>

        Remove the bugtracker associated with <abbreviation> from the list of
        defined gittrackers.
        """
        try:
            name = self.shorthand[name.lower()]
            del self.db[name]
            self.registryValue('gittrackers').remove(name)
            self.shorthand = utils.abbrev(list(self.db.keys()))
            irc.replySuccess()
        except KeyError:
            s = self.registryValue('replyNoBugtracker', ircutils.isChannel(msg.args[0]) and msg.args[0] or None)
            irc.error(s % name)
    remove = wrap(remove, [('checkCapability', 'admin'), 'text'])

    def rename(self, irc, msg, args, oldname, newname, newdesc):
        """<oldname> <newname>

        Rename the bugtracker associated with <oldname> to <newname>.
        """
        try:
            name = self.shorthand[oldname.lower()]
            group = self.trackerConfigGroup(name)
            d = group.description()
            if newdesc:
                d = newdesc
            self.db[newname] = defined_gittrackers[group.trackertype()](name,group.url(),d)
            registerGitTracker(newname, group.url(), d, group.trackertype(), group.apikey())
            del self.db[name]
            self.registryValue('gittrackers').remove(name)
            self.shorthand = utils.abbrev(list(self.db.keys()))
            irc.replySuccess()
        except KeyError:
            s = self.registryValue('replyNoBugtracker', ircutils.isChannel(msg.args[0]) and msg.args[0] or None)
            irc.error(s % name)
    rename = wrap(rename, [('checkCapability', 'admin'), 'something','something', additional('text')])

    def list(self, irc,  msg, args, name):
        """[abbreviation]

        List defined gittrackers. If [abbreviation] is specified, list the
        information for that bugtracker.
        """
        if name:
            name = name.lower()
            try:
                name = self.shorthand[name]
                (url, description, type) = (self.db[name].url, self.db[name].description,
                                            self.db[name].__class__.__name__)
                irc.reply('%s: %s, %s [%s]' % (name, description, url, type))
            except KeyError:
                s = self.registryValue('replyNoBugtracker', ircutils.isChannel(msg.args[0]) and msg.args[0] or None)
                irc.error(s % name)
        else:
            if self.db:
                trackers = list(self.db.keys())
                trackers.sort()
                irc.reply(utils.str.commaAndify(trackers))
            else:
                irc.reply('I have no defined gittrackers.')
    list = wrap(list, [additional('text')])

    def bugSnarfer(self, irc, msg, match):
        r"""\b(?P<bt>(([a-z0-9]+)?\s+bugs?|[a-z0-9]+)):?\s+#?(?P<bug>\d+(?!\d*[\-\.]\d+)((,|\s*(and|en|et|und|ir))\s*#?\d+(?!\d*[\-\.]\d+))*)"""
        # TODO: add support for gitlab bugs GNOME/gnome-shell#500
        # or channel-context based references
        pass

    def urlSnarfer(self, irc, msg, match):
        r"(?P<tracker>https?://\S*?)/(?:issues|pull|merge_requests)/(?P<bug>\d+)"
        channel = ircutils.isChannel(msg.args[0]) and msg.args[0] or None
        if not self.registryValue('urlSnarfer', channel):
            return
        nbugs = msg.tagged('nbugs')
        if not nbugs: nbugs = 0
        if nbugs >= 5:
            return
        msg.tag('nbugs', nbugs+1)
        try:
            tracker = self.get_tracker(match.group(0))
            if not tracker:
                return
            report = self.get_bug(channel, tracker, int(match.group('bug')),
                                    self.registryValue('showassignee', channel),
                                    do_url=False)
        except GitTrackerError as e:
            irc.error(str(e))
        except BugNotFoundError as e:
            irc.error("%s issue %s not found" % (tracker, match.group('bug')))
        else:
            for r in report:
                irc.reply(makeClean(r), prefixNick=False)

    def get_tracker(self, snarfurl):
        snarfhost = snarfurl.replace('http://','').replace('https://','')

        # At this point, we are only interested in the host part of the URL
        if '/' in snarfurl:
            snarfhost = snarfhost[:snarfhost.index('/')]

        for t in list(self.db.keys()):
            tracker = self.db.get(t, None)
            if not tracker:
                self.log.error("No tracker for key %r" % t)
                continue
            url = tracker.url.replace('http://','').replace('https://','')

            if '/' in url:
                url = url[:url.index('/')]
            if url in snarfhost:
                if tracker.has_multiple_trackers():
                    return tracker.get_tracker(snarfurl)
                return tracker

        return None

    def get_bug(self, channel, tracker, id, do_assignee, do_url = True, show_tracker = True):
        reports = []
        if not self.is_ok(channel, tracker, id):
            return []
        for r in tracker.get_bug(id):
            showext = self.registryValue('extended', channel)
            extinfo = ''
            if len(r) == 8:
                (bid, product, title, severity, status, assignee, url, extinfo) = r
            else:
                (bid, product, title, severity, status, assignee, url) = r

            state_infos = ', '.join([f for f in [severity.title(), status.title()] if f])
            tracker_name = tracker.description + ' '
            if not do_url:
                url = ''
            if not show_tracker:
                tracker_name = ''
            if product:
                if showext:
                    reports.append("%sissue %s in %s \"%s\" %s [%s] %s" % (tracker_name, bid, product,
                                                                          title, extinfo, state_infos, url))
                else:
                    reports.append("%sissue %s in %s \"%s\" [%s] %s" % (tracker_name, bid, product,
                                                                          title, state_infos, url))
            else:
                if showext:
                    reports.append("%sissue %s \"%s\" %s [%s] %s" % (tracker_name, bid, title, extinfo, state_infos, url))
                else:
                    reports.append("%sissue %s \"%s\" [%s] %s" % (tracker_name, bid, title, state_infos, url))
            if do_assignee and assignee:
                reports[-1] = reports[-1] + (" - Assigned to %s" % assignee)
        return reports

# Define all gittrackers
class IGitTracker:
    def __init__(self, name=None, url=None, description=None):
        self.name        = name
        self.url         = url
        self.description = description
        self.log         = supylog # Convenience log wrapper

    def get_bug(self, id):
        raise GitTrackerError("GitTracker class does not implement get_bug")

    def has_multiple_trackers(self):
        return False

    def get_tracker(self, url):
        if self.has_multiple_trackers():
            raise GitTrackerError("GitTracker class does not implement get_tracker")

    def __str__(self):
        return '%s(%s)' % (self.__class__.__name__, self.url)

    def __hash__(self):
        return hash(self.url)

    def __cmp__(self, other): # used implicitly in GitTracker.is_ok()
        return cmp(hash(self), hash(other))

    def __str__(self):
        return self.name

class Github(IGitTracker):
    _gh = None

    def __init__(self, *args, **kwargs):
        IGitTracker.__init__(self, *args, **kwargs)

        if not Github._gh:
            try: # We need python-github
                from github import Github as GH
                Github._gh = GH(user_agent='PyGithub/ubuntu-bot')
            except ImportError:
                self.log.error("Please install python-github")

        self.gh = Github._gh
        self.repo = None

    def has_multiple_trackers(self):
        return True

    def get_tracker(self, url):
        if not self.gh:
            return
        if not url:
            self.log.info("Impossible to get tracker for an empty url")
            return

        try:
            url = url.replace('http://', 'https://', 1)
            if url.startswith(self.url):
                projectmatch = re.search("([^/]+/[^/]+)/(issues|pull)/\d+", url)

                if projectmatch:
                    repo_name = projectmatch.group(1)
                    repo = self.gh.get_repo(repo_name)
                    if not repo.has_issues:
                        s = 'Repository {} has no issues'.format(repo_name)
                        self.log.exception(s)

                    gh = Github(repo.full_name.replace('/', '-'), repo.html_url, repo.owner.login)
                    gh.repo = repo
                    return gh

        except Exception as e:
            s = '{}: Impossible to get tracker for {}: {}'.format(
                self.description, url, e)
            self.log.exception(s)

    def get_bug(self, id):
        if not self.gh or not self.repo:
            self.log.info("Github or repository not initialized")
            return []

        try:
            issue = self.repo.get_issue(id)

            assignee = None
            labels = ', '.join([l.name for l in issue.labels])
            is_pull_request = issue.pull_request != None

            if issue.assignee:
                assignee = '{} ({})'.format(issue.assignee.login, issue.assignee.name)
            if is_pull_request:
                id = '(Pull request) {}'.format(id)

            # extinfo = '(comments: {}, reactions: {})'.format(issue.comments, len(issue.get_reactions()))
            extinfo = '(comments: {})'.format(issue.comments)

            return [(id, self.repo.name, issue.title, labels, issue.state, assignee, issue.html_url, extinfo)]
        except Exception as e:
            s = '{}: Impossible to get infos for {} issue {}: {}'.format(
                self.description, self.repo.full_name, id, e)
            self.log.exception(s)
            raise GitTrackerError(s)


class Gitlab(IGitTracker):
    _gl = {}

    def __init__(self, *args, **kwargs):
        IGitTracker.__init__(self, *args, **kwargs)

        self.gl = None
        self.project = None
        self.merge_requests = False
        self.hostname = self.url.split('://', 1)[1].split('/', 1)[0]

        if not self.hostname in list(Gitlab._gl.keys()):
            apikey = conf.supybot.plugins.GitTracker.gittrackers.get(self.name).apikey()
            if not apikey:
                self.log.error("No access token for gitlab instance {} ({})".format(
                    self.name, self.hostname))
                return

            try: # We need python-gitlab
                import gitlab
                from distutils.version import LooseVersion

                if not hasattr(gitlab, '__version__') or \
                   LooseVersion(gitlab.__version__) < LooseVersion('1.0.0'):
                    raise GitTrackerError("gitlab version must be 1.0.0 or newer")

                Gitlab._gl[self.name] = gitlab.Gitlab(self.url, private_token=apikey)
            except (ImportError, GitTrackerError):
                self.log.error("Please install python-gitlab (=> 1.0.0)")
                return

        self.gl = Gitlab._gl[self.name]

    def has_multiple_trackers(self):
        return True

    def get_tracker(self, url):
        if not self.gl:
            return
        if not url:
            self.log.info("Impossible to get tracker for an empty url")
            return

        try:
            url = url.replace('http://', 'https://', 1)
            site_prefix = self.url if self.url[-1] != '/' else self.url[:-1]

            if not url.startswith(site_prefix+'/'):
                return

            escaped_host = re.escape(self.hostname)
            uri_kinds_re = "(issues|merge_requests)\/\d+"
            projectmatch = re.search("{}\/(.+)\/-\/{}".format(
                escaped_host, uri_kinds_re), url)

            if not projectmatch:
                projectmatch = re.search("{}\/(.+)\/{}".format(
                    escaped_host, uri_kinds_re), url)

            if projectmatch:
                project_full_name = projectmatch.group(1)
                project = self.gl.projects.get(project_full_name)
                merge_request = projectmatch.group(2) == 'merge_requests'

                if not merge_request and not project.issues_enabled:
                    s = 'Project {} has no issues'.format(project_full_name)
                    self.log.error(s)
                    return
                elif merge_request and not project.merge_requests_enabled:
                    s = 'Project {} has no merge requests'.format(project_full_name)
                    self.log.error(s)
                    return

                description = project.name
                if project.namespace['kind'] != 'user':
                    description = project.namespace['name']

                gl = Gitlab(self.name, project.web_url, description)
                gl.project = project
                gl.merge_requests = merge_request
                return gl

        except Exception as e:
            s = '{}: Impossible to get tracker for {}: {}'.format(
                self.description, url, e)
            self.log.exception(s)

    def get_bug(self, id):
        if not self.gl or not self.project:
            self.log.info("Gitlab or project not initialized")
            return []

        try:
            if self.merge_requests:
                issue = self.project.mergerequests.get(id)
                id = '(Merge request) {}'.format(id)
            else:
                issue = self.project.issues.get(id)

            assignee = None
            labels = ', '.join([l for l in issue.labels])

            if issue.assignee and hasattr(issue.assignee, 'username') and 'name' in issue.assignee:
                assignee = '{} ({})'.format(issue.assignee.username, issue.assignee['name'])

            extinfo = '(comments: {})'.format(issue.user_notes_count)

            return [(id, self.project.path, issue.title, labels, issue.state, assignee, issue.web_url, extinfo)]
        except Exception as e:
            s = '{}: Impossible to get infos for {} issue {}: {}'.format(
                self.description, self.project.path_with_namespace, id, e)
            self.log.exception(s)
            raise GitTrackerError(s)


# Introspection is quite cool
defined_gittrackers = {}
v = vars()
for k in list(v.keys()):
    if type(v[k]) == type(IGitTracker) and issubclass(v[k], IGitTracker) and not (v[k] == IGitTracker):
        defined_gittrackers[k.lower()] = v[k]

tryRegisterGitTracker('freedesktop', 'https://gitlab.freedesktop.org', 'Freedesktop', 'gitlab')
tryRegisterGitTracker('github', 'https://github.com', 'Github', 'github')
tryRegisterGitTracker('gitlab', 'https://gitlab.com', 'Gitlab', 'gitlab')
tryRegisterGitTracker('gnome', 'https://gitlab.gnome.org', 'Gnome', 'gitlab')
tryRegisterGitTracker('salsa', 'https://salsa.debian.org', 'Debian Salsa', 'gitlab')

# Don't delete this one
Class = GitTracker
